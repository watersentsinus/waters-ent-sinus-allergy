Our practice specializes in traditional ear, nose, and throat problems such as ear infections, tonsil and adenoid infection and obstruction, hoarseness, snoring, vertigo, skin lesions and cancers, and the treatment of allergy and sinus disorders.

Address: 1913 Chester Blvd, Richmond, IN 47374, USA

Phone: 765-965-1977
